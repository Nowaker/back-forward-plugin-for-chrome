/* 	Credits to jasonsavard@gmail.com for this code. */

var DEBUG = true;
var subsequentLoad = false;
var metaStartPressed = false;

function keyDown(e) {
	if (e.ctrlKey && e.altKey) {
		if (e.which == 37) { // left
			chrome.extension.sendRequest({action: "back"}, function(response) {});
		} else if (e.which == 39) { // right
			chrome.extension.sendRequest({action: "forward"}, function(response) {});
		}
	}
}

if (!subsequentLoad) {
	document.addEventListener('keydown', keyDown, true);
}

subsequentLoad = true;

