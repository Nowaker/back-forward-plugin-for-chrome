var tabsHistory = new Array();
var tabsHistoryIndex = -1;

chrome.tabs.onSelectionChanged.addListener(
	function(tabId, windowId) {
		if (tabsHistory[tabsHistoryIndex] != tabId)
		{
			// Remove the elements with greater index than currentIndex
			tabsHistory.splice(tabsHistoryIndex + 1, tabsHistory.length - (tabsHistoryIndex + 1));
			
			tabsHistory.push(tabId);
			tabsHistoryIndex++;
		}

		UpdateIcon();
	});

chrome.tabs.onRemoved.addListener(
	function(tabId) {
		while (tabsHistory.indexOf(tabId) !=-1)
		{
			var removedIndex = tabsHistory.indexOf(tabId);
			tabsHistory.splice(removedIndex, 1);
			
			if (removedIndex <=tabsHistoryIndex)
				tabsHistoryIndex--;
		}
		UpdateIcon();
	});

function handler(request) {
	if (request.action == 'back') {
		if (tabsHistoryIndex > 0) {
			tabsHistoryIndex--;
		}
	} else if (request.action == 'forward') {
		if (tabsHistoryIndex < tabsHistory.length) {
			tabsHistoryIndex++;
		}
	}
	chrome.tabs.update(tabsHistory[tabsHistoryIndex], {selected:true})
}

chrome.extension.onRequest.addListener(handler);
chrome.browserAction.onClicked.addListener(handler);

chrome.extension.onRequestExternal.addListener(
  function(request, sender, callback) {
	if (request.action == 'tabfoward') {
		if (tabsHistoryIndex<tabsHistory.length-1)
		{
			tabsHistoryIndex++;
			return callback({'success': true, 'tabid': tabsHistory[tabsHistoryIndex], 'tabsHistoryIndex': tabsHistoryIndex, 'tabsHistoryLength' : tabsHistory.length });
		}
		else
		{
			return callback({'success': false });
		}
	}
	if (request.action == 'updatefowardicon') {
		return callback({'success': true, 'tabid': tabsHistory[tabsHistoryIndex], 'tabsHistoryIndex': tabsHistoryIndex, 'tabsHistoryLength' : tabsHistory.length });
	}	  
  });

function UpdateIcon()
{
	var iconPath;
	if (tabsHistoryIndex <= 0)
	{
		iconPath = 'back-16-shadow.png';
	}
	else
	{
		iconPath = 'back-16.png';		
	}
	chrome.browserAction.setIcon({'path':iconPath});
}

UpdateIcon();
console.log('tabback.js executed');
